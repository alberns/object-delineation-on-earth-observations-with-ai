Object Delineation on Earth Observations with AI.
It is a set of command-line tools performing semantic segmentation on remote
sensing images (aerial and/or satellite) with as many layers

## Installation

These instructions assume that you already have [conda](https://conda.io/) installed.





## Quickstart

 toolkit is run through main command:
```bash

usage: ode [-h] -c CONFIG [-v] {sample_grid,trainer}
odeon: error: the following arguments are required: tool, -c/--config
```

Each tool needs a specific JSON configuration file. Available schemas can be found in `odeon/scripts/json_defaults` folder.

More information is available in `docs` folder
